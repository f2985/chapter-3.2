describe('Code once, runs forever, Chapter 3.2', () => {

    beforeEach(function() {
        cy.visit('https://flip.id/landing').wait(1000)
        cy.viewport(1440,900).wait(500)
        
    })

    it('Flip generates revenue', () => {
      
        cy.get('.sc-gpHHfC').should('exist').click().wait(1000)
        cy.get('.sc-jrIrqw.hfiYef').should('contain.text', 'Flip+').should('contain.text', 'Flip for Business').should('contain.text', 'produk digital')

    });

    it('Switch the language', () => {
      
        cy.get('.sc-kPVwWT.jhfOdj').should('exist').click().wait(1000)

        cy.get('.navbar-nav .jsSttb').then(($bahasa) => { 

            if ($bahasa.text().includes('English')){

                cy.get($bahasa).click().wait(500)
                cy.get('[value="id"] > .sc-bsbRJL').click().wait(500)
                cy.get($bahasa).should('have.text', 'Indonesia')
                cy.get('.sc-dHmInP').should('have.text', 'Hai!')

            } else {

                cy.get($bahasa).click().wait(500)
                cy.get('[value="en"] > .sc-bsbRJL').click().wait(500)
                cy.get($bahasa).should('have.text', 'English')
                cy.get('.sc-dHmInP').should('have.text', 'Hello there!')

            }

        })  
        
    });

    it('Help section', () => {

        const getIframeDocument = () => {
            return cy
            .get('iframe[name="fc_widget"]')
            .its('0.contentDocument').should('exist')
          }
          
        const getIframeBody = () => {
            // get the document
            return getIframeDocument()
            // automatically retries until body is loaded
            .its('body').should('not.be.undefined')
            // wraps "body" DOM element to allow
            // chaining more Cypress commands, like ".find(...)"
            .then(cy.wrap)
        }

        getIframeBody().find('.chat-content').should('exist').click()
        getIframeBody().find('.search-category').should('exist').click()
        getIframeBody().find('.search-input-text').type('e-Wallet')
        getIframeBody().find('.category-item').contains('Bank yang di Support oleh Flip').click()
        getIframeBody().find('.modal-body').should('contain.text', 'OVO, DANA, Gopay, Shopeepay')
        
    });

});
