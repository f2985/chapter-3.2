# Chapter 3.2



## Getting started

##### Make sure you already install npm in your computer

##### Install Cypress
```shell
cd /your/project/path 
```
```shell
npm install cypress --save-dev 
```

##### If you're not using Node or npm in your project or you want to try Cypress out quickly, you can always download [Cypress directly from our CDN](https://download.cypress.io/desktop).

##### Copy file "flip-test.spec.js" to Integration folder under cypress folder

![](https://d1ro8r1rbfn3jf.cloudfront.net/ms_449357/oeM6KxGx0KU0NVTC2OEfUNNwzuhgm4/flip-test.spec.js%2B-%2Bchapter-3.2%2B-%2BVisual%2BStudio%2BCo.png?Expires=1648882800&Signature=Onyo9Eb9rlhy8tnaXAAjo8Xg8anWS1xA-kS-NbFjQ5jvTIsR6Hc7KXwRtW15967cKJYd7hy8ok5M1rbXHiFYqwS8RpxLmb1naEYgUfbbo0OI2Bjs8pq9rg2Oqp4xLlYqZWYlARf2aAN-P61VL6ANF8cBFDJWoLjLS9fgkEcWY7q9~VydMhCF2JcAdk2rlvqPqTkKdUsEacKpgIAUzm3A3taiRef74PiJgQuugP0UUt292V6Hf-70H4ci7HHEA-wWXYUKAwTmOav1XPQhkwFgXYRy5F3eVFHhV8HX3laU5EkVzjLpVPpGDUHSJdbDrYQuvelJhk5EpummceqoaBm5nA__&Key-Pair-Id=APKAJBCGYQYURKHBGCOA)

##### Run the test

- npx cypress open

- after cypress opened, choose file "flip-test.spec.js" 
